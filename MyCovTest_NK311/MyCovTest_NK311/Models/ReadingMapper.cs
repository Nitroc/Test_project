﻿using CsvHelper.Configuration;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MyCovTest_NK311.Models
{
    public sealed class ReadingMapper : ClassMap<MeterReadingDetails>
    {
        public ReadingMapper()
        {
            Map(x => x.AccountId).Name("AccountId");
            Map(x => x.MeterReadingDateTime).Name("MeterReadingDateTime");
            Map(x => x.MeterReadValue).Name("MeterReadValue");
           
        }
    }
}
