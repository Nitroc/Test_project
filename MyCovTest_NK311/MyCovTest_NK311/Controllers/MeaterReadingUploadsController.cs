﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Net.Http.Headers;
using System.Text.Json;
using System.Threading.Tasks;
using CsvHelper;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using MyCovTest_NK311.Models;
using Newtonsoft.Json;

using System.Collections;
using Newtonsoft.Json.Serialization;
using System.Data.Common;

namespace MyCovTest_NK311.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class MeaterReadingUploadsController : ControllerBase
    {
        public AppDb Db { get; set; }
        public MeaterReadingUploadsController(AppDb db)
        {
            Db = db;
        }
        [HttpPost, DisableRequestSizeLimit]
        public async Task<IActionResult> Upload()
        {
            try
            {
                var formCollection = await Request.ReadFormAsync();
                var file = formCollection.Files.First();
                // var file = Request.Form.Files[0];    
                var folderName = Path.Combine("Files", "csvfiles");
                var pathToSave = Path.Combine(Directory.GetCurrentDirectory(), folderName);
                if (file.Length > 0 && Path.GetExtension(ContentDispositionHeaderValue.Parse(file.ContentDisposition).FileName).Trim('"').ToLower() == ".csv")
                {
                    

                    var fileName = (System.IO.Path.GetFileNameWithoutExtension(
                        ContentDispositionHeaderValue.Parse(file.ContentDisposition).FileName) +
                        DateTime.Now.ToString("ddmmyyhmmss") +
                        Path.GetExtension(ContentDispositionHeaderValue.Parse(file.ContentDisposition).FileName)).Trim('"');

                    var fullPath = Path.Combine(pathToSave, fileName);
                    var dbPath = Path.Combine(folderName, fileName);
                    using (var stream = new FileStream(fullPath, FileMode.Create))
                    {

                        file.CopyTo(stream);
                    }
                    string strMessage = ReadCSVFile(fullPath);
                    return Ok(new { strMessage });
                }
                else
                {
                    return BadRequest("not a csv file.");
                }
            }
            catch (Exception ex)
            {
                return StatusCode(500, $"Internal server error: {ex}");
            }
        }



        public string ReadCSVFile(string location)
        {
            try
            {
                using (var reader = new StreamReader(location))
                {
                    List<MeterReadingDetails> lstMeterReadingDetails = new List<MeterReadingDetails>();
                    string strMessage = "";
                    using (var csv = new CsvReader(reader, CultureInfo.InvariantCulture))
                    {
                        lstMeterReadingDetails = csv.GetRecords<MeterReadingDetails>().ToList();

                        ;
                        List<MeterReadingDetails> lstNonMeterReadings = new List<MeterReadingDetails>();
                        List<MeterReadingDetails> lstsavedMeterReadings = new List<MeterReadingDetails>();

                        foreach (MeterReadingDetails objMeterReadingDetails in lstMeterReadingDetails)
                        {
                            Db.Connection.Open();
                            using var cmd = Db.Connection.CreateCommand();
                            cmd.CommandText = @"SELECT `account_id` FROM `test_accounts` where account_id='" + objMeterReadingDetails.AccountId + "' ;";
                            DbDataReader read = cmd.ExecuteReader();

                            if (read.HasRows)
                            {
                                Db.Connection.Close();
                                Db.Connection.Open();

                                cmd.CommandText = @"INSERT INTO `meter_readings` (`account_id`, `reading_date`, `meter_reading`) VALUES
                                                ('" + objMeterReadingDetails.AccountId + "','"
                                                + objMeterReadingDetails.MeterReadingDateTime + "','"
                                                + objMeterReadingDetails.MeterReadValue + "'); ";

                                cmd.ExecuteNonQuery();
                                Db.Connection.Close();

                                lstsavedMeterReadings.Add(objMeterReadingDetails);
                            }
                            else
                            {
                                lstNonMeterReadings.Add(objMeterReadingDetails);
                                Db.Connection.Close();
                            }

                        }

                        strMessage = "Total Records Received :" + lstMeterReadingDetails.Count() + "," +
                        "Total successfully processed unique Records :" + lstsavedMeterReadings.Select(a => a.AccountId).Distinct().Count() + "," +
                        "Total unsuccessfull unique records :" + lstNonMeterReadings.Select(a => a.AccountId).Distinct().Count();


                    }

                    return strMessage;
                }
            }
            catch (Exception e)
            {
                throw new Exception(e.Message);
            }
        }



    }
}
