import { Component, EventEmitter, Output } from '@angular/core';
import { HttpEventType, HttpClient } from '@angular/common/http';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css'],
})
export class AppComponent {

  public response: {dbPath: ''};
  public uploadFinished = (event) => {
    this.response = event;
  }
  public createImgPath = (serverPath: string) => {
    return `http://localhost:5200/${serverPath}`;
  }
  onCreate(){
  // this.user = {
  //   name: this.name,
  //   address: this.address,
  //   imgPath: this.response.dbPath
  // };
  }
}

